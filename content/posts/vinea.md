---
title: 拜恩
subtitle: Vinea
seal: 45-Vine_seal.png
---

拜恩（或称瓦因），位阶地狱伯爵兼君主，他统帅三十六个恶魔军团。他通博古今，极往知来；发现巫师和隐秘之物；制造滔天风暴；推倒城墙，建起高塔。这位魔神被描述为一头手部持蛇，骑在黑马上的狮子。
