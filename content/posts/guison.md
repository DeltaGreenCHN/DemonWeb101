---
title: 古辛
subtitle: Guison
seal: 11-Guison_seal.png
---

古辛位阶地狱大公爵，统帅四十个恶魔军团。他博古通今，极往知来，对问题知无不言，言无不尽。他能使友人和谐相处，并赐予荣誉与尊严。
