---
title: 瓦布拉
subtitle: Vapula
seal: 60-Vapula_seal.png
---

瓦布拉是一位强悍的地狱大公爵，统帅三十六个恶魔军团。他向巫师传授哲学、机械学和自然科学。瓦布拉被描绘为一头长着狮鹫翅膀的狮子。
