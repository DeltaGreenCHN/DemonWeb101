---
title: 毕弗隆斯
subtitle: Bifrons
seal: 46-Bifrons_seal.png
portrait: 220px-Bifrons copy.png
---

比弗隆斯位阶地狱伯爵，统帅六个恶魔军团。他信奉科学、艺术、宝石，草药以及森林的美德；他善用秘法将死者从一个坟墓迁徙到另一个坟墓，在他所控制的死者的坟墓上点燃魔法蜡烛，以惊吓活人；他起初的形象看起来像个怪物，但后世又变作了人形。
