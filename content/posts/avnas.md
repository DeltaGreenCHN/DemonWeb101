---
title: 阿伏那
subtitle: Avnas
seal: Avnas Amy seal.png
---

阿伏那（或称阿米）位阶地狱统领，统帅三十六个恶魔军团。他教授天文，给予使魔，煽动恐惧，揭露宝藏。起初他总是以一团烈焰的形象出现，后世化为人形。
