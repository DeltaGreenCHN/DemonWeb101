---
title: 巴巴托斯
subtitle: Barbatos
seal: 08-Barbatos_seal.png
portrait: 220px-Ill_dict_infernal_p0093-79_barbatos.png
---

巴巴托斯位阶地狱伯爵与公爵，统帅三十个恶魔军团，座下有四位地狱之王管辖他的军团。他能理解动物的语言，揭示过去和未来，调和友谊和统治的关系，并带领人们找到被巫师的魔法所掩盖隐藏的宝藏。
