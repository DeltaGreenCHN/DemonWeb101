---
title: 玛帕斯
subtitle: Malphas
seal: 39-Malphas.png
portrait: 220px-Malthas.png
---

玛帕斯位阶为地狱大亲王，统帅四十个恶魔军团。玛帕斯垒屋建塔，向召唤者之敌掷石。他摧毁敌人的思想与欲望；赐予优良使魔；迅速聚集世界各地的工匠。如若不是必须扮成常人的模样，他便会以乌鸦的形态出现。
