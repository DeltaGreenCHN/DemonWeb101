---
title: 比列
subtitle: Belial
seal: 68-Belial_seal.png
portrait: 300px-Belial.png
---

比列位阶地狱统领，统帅八十个恶魔军团和五十个鬼灵军团。路西法堕天后，他首先组织响应；他有权授予亲信以官职地位，并赠予优秀使魔；向他请问时，必须供以供品、牲品和祭物，否则他不会给出正确答复。
