---
title: 阿加雷斯
subtitle: Agares
seal: 02-Agares_seal.png
portrait: 200px-Aguares.png
---

阿加雷斯统治地狱之东，统帅三十一个恶魔军团。他能唤回逃亡之人，也能令静止之人逃跑，以教授不齿之事为趣，以摧毁世俗与超然的高贵之质为乐。他的现身形象为一个骑着鳄鱼的苍白老人。
