---
title: 吉蒙里
subtitle: Gremory
seal: 56-Gremory_seal.png
portrait: gremory Ill_dict_infernal_p0323-307_gomory_demon.png
---

吉蒙里是统帅二十六个恶魔军团的强大地狱公爵。他博古通今，极往知来，诉以真言，搜寻宝藏，博得芳心。他的形象为骑着单峰驼的，腰间系着公爵夫人冠冕的美人。
