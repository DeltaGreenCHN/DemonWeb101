---
title: 比利士
subtitle: Berith
seal: 28-Berith_seal.png
---

比利士是强大恐怖的地狱公爵，统帅二十六个恶魔军团。他博古通今，极往知来；点石成金，为人提供社会要职并稳固他们的身份地位；他的声音清晰而含蓄，但他不回答问题时只会口吐谎言；他的形象为身着红衣，头戴金冠，脚骑红马的士兵。
