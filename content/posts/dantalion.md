---
title: 但他林
subtitle: Dantalion
seal: 71-Dantalion_seal.png
---

但他林是一位威严的大公爵，他以数种男女面貌示人。他的右手常持一本书；他的职责是向人传授所有的艺术和科学，他能够探知任何男女的隐私，并随心改变它们；他能激发爱意，模仿出任意模样，并以异象或征兆显示——无论距离多么遥远。他统帅三十六个鬼灵军团。
