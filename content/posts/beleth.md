---
title: 贝雷特
subtitle: Beleth
seal: 13-Beleth_seal01.png
---

贝雷特是一位雄壮而可怕的地狱之王，统帅八十五个恶魔军团。他善骑战马，乐器齐奏即宣告他将驾临；贝雷特将使受巫师追求的男女心中的爱意萌生，直至此凡人满足于此；大洪水后，诺亚之子首先召唤了他，并被赐予了数学知识。
