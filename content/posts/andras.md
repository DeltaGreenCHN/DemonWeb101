---
title: 安托士
subtitle: Andras
seal: 63-Andras_seal.png
portrait: 220px-Andras(demon).png
---

安托士位阶地狱大侯爵，形象形似天使，头似林枭，挥舞利剑，身骑黑狼。他是引起意见分歧的祸首，能杀死主人、仆人以及所有助手。他统领着三十三个恶魔军团。
