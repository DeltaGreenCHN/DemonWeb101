---
title: 冒索斯
subtitle: Malthus
seal: malthus or halphas.png
---

冒索斯（或称哈法斯）位阶地狱伯爵，统帅二十六个恶魔军团。据说这位形似鹳鸟的魔神声音粗哑；冒索斯建造的塔楼通常会荷枪实弹，以作为自己在地狱之军械库；他位阶地狱王子。据说他还会派遣亲卫军团参加战争，或者前往更高阶魔神指定之处。
