---
title: 阿蒙
subtitle: Aamon
seal: 07-Amon_seal.png
portrait: 300px-Aamon.png
---

阿蒙位阶地狱侯爵，统帅四十个恶魔军团。他身现两种形象，一是口吐烈焰的蛇尾狼；二是长着狗牙的人身渡鸦头。他博古通今，极往知来。他能挑起仇怨，亦能调和敌人或朋友之间的争端。
