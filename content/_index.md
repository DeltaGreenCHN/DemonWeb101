## 普利米乌玛顿的初召仪式

“普利米乌玛顿”在希腊语中意为“既为起始、亦为终结之汝”。初召仪式以天使与耶和华的尊名，祈求在指挥恶魔之力行使危险工作时得以庇护。

祈求仪式需有精心准备：一把剑、用于占卜与施术的器皿、蜡像、由血书写的卷轴、一枚戒指、一根仪式杖、特殊服饰、一个花环。除此之外，还需根据奥妙的占星规则精心挑选合适的时机。

在一个与外界隔离的封闭区域内，绘制一个所罗门三角，并写上“ANAPHAXETON, ANAPHANETON, PRIMEUMATON”三段文字，作为保护阵。

<img src="images/solomonic-triangle-copy.png" style="display: block; margin-left: auto; margin-right: auto;">


此后，留在圈内念诵下列咒文：

{{% blockquote %}}
Thee I invoke, the Bornless one.

Thee, that didst create the Earth and the Heavens:

Thee, that didst create the Night and the day.

Thee, that didst create the darkness and the Light.

Thou art Osorronophris: Whom no man hath seen at any time.

Thou art Iabos:

Thou art Iapos:

Thou hast distinguished between the just and the Unjust.

Thou didst make the female and the Male.

Thou didst produce the Seed and the Fruit.

Thou didst form Men to love one another, and to hate one another.

I am Ank F N Khonsu Thy Prophet, unto Whom Thou didst commit Thy Mysteries, the Ceremonies of Khem:

Thou didst produce the moist and the dry, and that which nourisheth all created Life.

Hear Thou Me, for I am the Angel of Apophrasz Osorronophris: this is Thy True Name, handed down to the Prophets of Khem.

Hear Me: Ar: Thiao: Reibet: Atheleberseth: A: Blatha: Abeu: Eben: Phi: Chitasoe: Ib: Thiao.

Hear Me, and make all Spirits subject unto Me: so that every Spirit of the Firmament and of the Ether: upon the Earth and under the Earth: on dry Land and in the Water: of Whirling Air, and of rushing Fire: and every Spell and Scourge of God may be obedient unto Me.

I invoke Thee, the Terrible and Invisible God: Who dwellest in the Void Place of the Spirit: Arogogorobrao: Sochou: Modorio: Phalarchao: Ooo: Ape, The Bornless One: Hear Me!

Hear Me: Roubriao: Mariodam: Balbnabaoth: Assalonai: Aphniao: I: Tholeth: Abrasax: Qeoou: Ischur, Mighty and Bornless One! Hear Me! I invoke Thee: Ma: Barraio: Ioel: Kotha: Athorebalo: Abraoth: Hear Me!

Hear me! Aoth: Aboth: Basum: Isak: Sabaoth: Iao:

This is the Lord of the Gods:

This is the Lord of the Universe:

This is He Whom the Winds fear.

This is He, Who having made Voice by His Commandment, is Lord of All Things; King, Ruler, and Helper. Hear Me!

Hear Me: Ieou: Pur: Iou: Pur: Iaot: Iaeo: Ioou: Abrasax: Sabriam: Oo: Uu: Ede: Edu: Angelos tou theou: Lai: Gaia: Apa: Diachanna: Chorun. I am He! the Bornless Spirit! having sight in the Feet: Strong, and the Immortal Fire!

I am He! the Truth!

I am He! Who hate that evil should be wrought in the World!

I am He, that lightningeth and thundereth.

I am He, from whom is the Shower of the Life of Earth:

I am He, whose mouth flameth:

I am He, the Begetter and Manifester unto the Light:

I am He, the Grace of the World:

"The Heart Girt with a Serpent" is My Name!

Come Thou forth, and follow Me: and make all Spirits subject unto Me so that every Spirit of the Firmament, and of the Ether: upon the Earth and under the Earth: on dry land, or in the Water: of whirling Air or of rushing Fire: and every Spell and Scourge of God, may be obedient unto me!

Iao: Sabao: Such are the Words!
{{% /blockquote %}}

再念出七十二魔神之一的名讳，作为召唤行令的对象：

{{< demonlist >}}

将你希望接触的魔神印记刻录下来。

仪式完成后，魔神就会现身。
